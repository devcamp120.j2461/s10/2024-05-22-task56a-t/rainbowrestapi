package com.devcamp.rainbow.controllers;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

import com.devcamp.rainbow.services.RainbowService;

@Controller
public class RainbowController {

    @Autowired
    public RainbowService rainbowservice;

    @GetMapping("/rainbow")
    public ResponseEntity<ArrayList<String>> getRainbowList() {
        return new ResponseEntity<>(this.rainbowservice.getRainbowList(), HttpStatus.OK);
    }
}
