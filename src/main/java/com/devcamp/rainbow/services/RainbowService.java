package com.devcamp.rainbow.services;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

@Service
public class RainbowService {
    public ArrayList<String> getRainbowList() {
        String[] list = {"red", "orange", "yellow", "green", "blue", "indigo", "violet"};
        ArrayList<String> rainbowList = new ArrayList<>();

        for (String color: list) {
            rainbowList.add(color);
        }

        return rainbowList;
    }
}
